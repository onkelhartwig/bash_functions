#!/bin/bash
# ----------------------------------------------------------------------------------
# Collection of functions to do date mathematics and formatting.
# to make things easy a standard format for date strings is required 
# which is ISO-8601. This format is always the output for dates.
#   Example:  2022-06-07T17:30:40+02:00
#             %Y   %m %d %H %M %S Offset to UTC in hours:minutes 
#
# As input any format is supported that is accepted by the "--date" option
#  of the date command
# Here some examples:
#    1. '+%Y-%m-%dT%H:%M:%S %Z'      - e.g. "2022-06-07 17:30:40 CEST"
#                                       this is NOT recommended, see below
#    2. ISO-8601                     - e.g. "2022-06-07T17:30:40+02:00"
#    3. ISO-8601 without UTC offset  - e.g. "2022-06-07 17:30:40" 
#    4. the current time             - e.g. "now", "today" or just nothing ""
#    5. only a date                  - e.g. "2022-04-03"
#    6. only a time                  - e.g. "18:00"
#    7. date for different time zone - e.g. 'TZ="Europe/London" 2022-06-09T18:00:00'
#                                       You need to know what you are doing here !
# For format 1. have in mind that "2022-06-07 17:30:40 CEST" is valid, but
#                                 "2022-06-07 17:30:40 CET"  is invalid
# because of daylight saving time
#
# Adding days/times will require to use the following format:
#    (+/-)D:H:M:S            - e.g. -1:03:00:00 to subtract 1 day and 3 hours
#                                   +1:03:00:00 to add 1 day and 3 hours  
#                                   1:03:00:00 to add 1 day and 3 hours  
#  
# Functions:
#    d8_easter(year)         - get Easter date
#    d8_std_form(date)       - bring a date in ISO format
#    d8_diff_date(young,old) - difference of 2 dates in days
#    d8_add_time(date,time)  - add time to a date. time has format
#                                                 [+/-]d:h:m:s
#    d8_cw(date)             - display ISO calendar week
#    d8_wd(date)             - display day number and weekday name (e.g. 4 Thursday)
#    d8_dy(date)             - display day of year
#    d8_fd(date)             - display only date part (YYYY-MM-DD)
#    d8_ot(date)             - display only time part (HH:MM:SS)
#    d8_split(date)          - split ISO date into array. Special considerations 
#                              must be followed to make that work:
#                              arr=$(d8_split now) ; echo ${arr[1]} ${arr[2]} ...
# ----------------------------------------------------------------------------------

d8_easter() {
# ----------------------------------------------------------------------------------
# d8_easter(year)
#  calculates the Easter date for give year "year"
# ----------------------------------------------------------------------------------
year=$1
a=$(( year % 19 ))
b=$(( year %4 ))
c=$(( year %7 ))
k=$(( year / 100 ))
p=$(( (13 + 8 * k) / 25 ))
q=$(( k / 4 ))
if [ $year -ge 1583 ]
then
   # Gregorian calendar
   M=$(( (15 - p + k - q) % 30 ))
   N=$(( (4 + k - q) % 7 ))
else
   # Julian calendar
   M=15
   N=6
fi
d=$(( (19 * a + M) % 30 ))
e=$(( (2 * b + 4 * c + 6 * d + N) % 7 ))
local easterday=$(( 22 + d + e ))
if [ $easterday -gt 31 ]
then
   easterday=$(( easterday - 31 ))
   eastermonth="04"
else
   eastermonth="03"
fi
if [ $d -eq 28 -a $e = 6 -a $(( ( 11*M+11 ) % 30 )) -lt 19 -a $easterday = 25 -a $eastermonth = "04" ]
then
   easterday=18
elif [ $d -eq 29 -a $e = 6 -a $easterday = 26 -a $eastermonth = "04" ]
then
   easterday=19   
fi
echo $year"-"$eastermonth"-"$easterday
}

d8_std_form() {
# ----------------------------------------------------------------------------------
# d8_std_form("date",[tz]) 
#  translates the given input time to date_func standard. If the translation fails
#  error message is display on stderr and "" is returned on stdout
#  "date" - can be any valid string for date command option "--date"
#      see the info page (not man page) to see details for the input date format. Here
#      the relevant lines:
#          Display the date and time specified in DATESTR instead of the
#          current date and time.  DATESTR can be in almost any common format.
#          It can contain month names, time zones, ‘am’ and ‘pm’, ‘yesterday’,
#          etc.  For example, ‘--date="2004-02-27 14:19:13.489392193 +0530"’
#          specifies the instant of time that is 489,392,193 nanoseconds after
#          February 27, 2004 at 2:19:13 PM in a time zone that is 5 hours and
#          30 minutes east of UTC.
#          Note: input currently must be in locale independent format.  E.g.,
#          the LC_TIME=C below is needed to print back the correct date in
#          many locales:
#               date -d "$(LC_TIME=C date)"
#          *Note Date input formats::.
#          if you get the message "invalid date", pls. check at least the following:
#             - time zone (e.q. date -d "2009-10-18 CET" is invalid, 
#               the time zone must be CEST)
#             - format of the date (e.g. 2022-10-18 is valid while 2022.10.18 is
#               invalid)
#  "tz" - target time zone, default is environment variable TZ. "tz" must be 
#         specified as the time zone name like Europe/Berlin, NOT as time zone 
#         abbreviation like "CET". UTC is an exception
#  to get the time in Calcutta use (current setting of TZ is "Europe/Berlin"
#         d8_std_form "18:30" "" "Asia/Calcutta"
#         ---> Result: 2022-06-09T15:00:00+02:00
#  to get the time in UTC use 
#         d8_std_form "18:30" "UTC" "Asia/Calcutta"
#         ---> Result: 2022-06-09T13:00:00+00:00
#  to get London time use
#         d8_std_form "18:30" "Europe/London" "Asia/Calcutta"
#         ---> Result: 2022-06-09T14:00:00+01:00
#  When comparing the results have an eye on the offset which represents the
#  output time zone.
#       
# ----------------------------------------------------------------------------------
local tzin tzout tzstring q a blank bs
q='"'
a="'"
blank=" "
bs='\'
[ "$2" = "" ] && tzout="$TZ" || tzout="$2"
[ "$3" = "" ] && tzin="" || tzin="$3"
if [ "$tzin" = "" ]
then
   TZ="$tzout" date --iso-8601="seconds" --date="$1"
else
   eval TZ="$tzout" date --iso-8601="seconds" --date=$a'TZ="'$tzin$q$blank$1$a
fi
# TZ="Asia/Calcutta" date --iso-8601=seconds --date='TZ="Europe/Berline" 18:00'
}

d8_diff_date() {
# ----------------------------------------------------------------------------------
# d8_diff_date(younger_date,older_date)
#  returns the difference between 2 dates in days, hours, minutes and seconds in
#  the following format:
#  (+/-)d:h:m:s
# ----------------------------------------------------------------------------------
local young older diff days hours minutes seconds sign
young="$1"
older="$2"
young="$(d8_std_form $young)"
older="$(d8_std_form $older)"
young=$(date --date "$young" +%s)
older=$(date --date "$older" +%s)
diff=$(( young - older ))
if [ ${diff:0:1} = "-" ]
then
   sign="-" 
   diff=${diff:1}
else
   sign="+"
fi
days=$(( diff / 86400 ))
diff=$(( diff - days * 86400 ))
hours=$(( diff / 3600 ))
diff=$(( diff - hours * 3600 ))
minutes=$(( diff / 60 ))
diff=$(( diff - minutes * 60 ))
seconds=$diff
echo "$sign$days:$hours:$minutes:$seconds"
}

d8_add_time() {
# ----------------------------------------------------------------------------------
# d8_add_time(date,tobeadded)
#  add the time "tobeadded" to "date" and display the result as date string
#  date - specifies the start date
#  tobeadded - specifies the time to be added in the format d:h:m:s, where
#              d - is the number of days
#              h - is the number of hours
#              m - is the number if minutes
#              s - is the number of seconds
#              If a value is zero you can omit the part but the separator ":"
#              must be specified. To subtract times specify the "-" without blanks
#              in front of "d"
#              Example: 2:3::12 will add 2 days, 3 hours and 12 seconds to date
#                       -:4::   will subtact 4 hours from date
# ----------------------------------------------------------------------------------
local days hours minutes seconds tims date sum sign
days=$(echo $2 | cut -d":" -f1)
if [ ${days:0:1} = "-" ]
then
   sign="-1" 
   days=${days:2}
elif [ ${days:0:1} = "+" ]
then
   sign="+1"
   days=${days:1}
else
   sign="+1"
fi
hours=$(echo $2 | cut -d":" -f2)
minutes=$(echo $2 | cut -d":" -f3)
seconds=$(echo $2 | cut -d":" -f4)
days=$(( days * 86400 ))
hours=$(( hours * 3600 ))
minutes=$(( minutes * 60 ))
time=$(( days+minutes+hours+seconds ))
date=$(d8_std_form "$1")
date=$(date --date "$1" +%s)
sum=$(( date + sign * time ))
d8_std_form @$sum 
}

d8_cw() {
# ----------------------------------------------------------------------------------
# d8_cw(date)
#  display ISO calendar week (Monday first day of month) of date
# ----------------------------------------------------------------------------------
date --date "$1" +%V
}

d8_wd() {
# ----------------------------------------------------------------------------------
# d8_wd(date)
#  display day of week (Monday is 1, Sunday is 7) and it's local name of date
# ----------------------------------------------------------------------------------
echo $(date --date "$1" +%u ; date --date "$1" +%A)
}

d8_dy() {
# ----------------------------------------------------------------------------------
# d8_dy(date)
#  display day of year of date
# ----------------------------------------------------------------------------------
date --date "$1" +%j
}

d8_dm() {
# ----------------------------------------------------------------------------------
# d8_dy(date)
#  display day of year of date
# ----------------------------------------------------------------------------------
date --date "$1" +%m
}

d8_fd() {
# ----------------------------------------------------------------------------------
# d8_fd(date)
#  display full date in format YYYY-MM-DD
# ----------------------------------------------------------------------------------
date --date "$1" +%F
}

d8_ot() {
# ----------------------------------------------------------------------------------
# d8_ot(date)
#  display only the time part in format HH:MM:SS
# ----------------------------------------------------------------------------------
date --date "$1" +%T
}

d8_split() {
# ----------------------------------------------------------------------------------
# d8_split(std_date)
#  returns an indexed array with the following content:
#     arr[0] - dummy
#     arr[1] - Year
#     arr[2] - Month
#     arr[3] - Day
#     arr[4] - Hour
#     arr[5] - Minutes
#     arr[6] - Seconds
#     arr[7] - time zone offset
# Call in the following way to make use of the array 
#    arr=($(split_std_date "2022-06-07T17:30:40+02:00")) ; echo ${arr[1]}
#    2022-06-07T17:30:40+02:00
# ----------------------------------------------------------------------------------
local Y m D H M S o stddate
stddate=$(d8_std_form "$1")
Y=${stddate:0:4}
m=${stddate:5:2}
D=${stddate:8:2}
H=${stddate:11:2}
M=${stddate:14:2}
S=${stddate:17:2}
o=${stddate:19}
echo "dummy" $Y $m $D $H $M $S $o 
}
