#!/bin/bash
# ==================================================================================
# This is a collection of bash implementations of rexx builtin functions.
# Idea is to make some additional functionality and/or better readability
# in scripts, cryptic syntax can be exchanged by more readable calls.
#
# All functions are using local variables only, not variable of caller is modified
#
# ------------------ !!   NOTE   !! ---------------------
# Incation is always as follows, in the list I used function syntax for better reading
#   var="$(func "param1" "param2" ...)"
# For documentation of wach funnction and it's parameters refer to the comment at the
# beginning of each function
# The functions do not check the given arguments to be valid, that's task of the 
# calling script
# ------------------ !! END NOTE !! ---------------------
#
# Implemented are:
#   rx_abbrev(long, short [,length])
#   rx_center(string,length,[padchar])
#   rx_changestr(needle, haystack, newneedle)
#   rx_compare(string1, string2 [,padchar])
#   rx_compress(string [,list])
#   rx_copies(str,n)
#   rx_countstr(needle, haystack)
#   rx_delstr(string, start [,length])
#   rx_delword(string,start[,length])
#   rx_index(haystack, needle [,start])
#   rx_lastpos(needle, haystack [,start])
#   rx_left(string, length [,padchar])
#   rx_length(string)
#   rx_lower(string [,start [,length [,pad]]])
#   rx_max(number [,number] ...)
#   rx_min(number [,number] ...)
#   rx_overlay(string1, string2 [,[start] [,[length] [,padchar]]])
#   rx_pos(needle, haystack [,start])
#   rx_reverse(string)
#   rx_right(string, length[,padchar])
#   rx_space(string[, [length] [,padchar]])
#   rx_strip(string [,[option] [,char]])
#   rx_substr(string,start,[length],[padchar])
#   rx_subword(string, start [,length])
#   rx_upper(string [,start [,length [,pad]]])
#   rx_verify(string, ref [,[option] [,start]])
#   rx_word(string, wordno)
#   rx_wordindex(string, wordno)
#   rx_wordlength(string, wordno)
#   rx_wordpos(phrase, string [,start])
#   rx_words(string)
#
# ==================================================================================

rx_abbrev() {
# ----------------------------------------------------------------------------
# rx_abbrev(long, short [,length])
#   Returns 1 if the string short is strictly equal to the leading charcaters
#   of the string long, and returns 0(zero) otherwise.
#   The minimum length which short must have, can be specified as length.
#   If length is unspecified, length is set to the length of short.
#   The nullstring is an abbreviation of any string.
# ----------------------------------------------------------------------------
local long="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local short="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local length=$(rx_length "$short") || local length=$3
if [ $length -gt "$(rx_length $short)" ]
then
   echo 0
   return 0
fi
if [ "$(rx_substr "$long" 1 $(rx_length "$short"))" = "$short" ]
then
   echo 1
   return 1
else
   echo 0
   return 0
fi
}

rx_center() {
# ----------------------------------------------------------------------------
# rx_center(string,length,[padchar])
#   positions string in a new string of lenght filled left and right by padchar
# ----------------------------------------------------------------------------
local str="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local los=${#str}
local mis=$(($2-$los))
if [ "$3" = "" ]
then local padchar=" "
else local padchar=$3
fi
local left=$(($mis/2))
if [ $(($left*2)) -lt $mis ]
then local right=$(($left+1))
else local right=$left
fi
echo "$(rx_copies "$padchar" $left)$str$(rx_copies "$padchar" $right)"
return $?
}

rx_changestr() {
# ----------------------------------------------------------------------------
# rx_changestr(needle, haystack, newneedle)
#   replaces all occurences of needle in haystack by newneedle
#   needle and newneedle can be sed regexp, a string is a regexp, too.
# ----------------------------------------------------------------------------
echo "$2" | sed -r 's/'$1'/'$3'/g'
return $?
}

rx_compare() {
# ----------------------------------------------------------------------------
# rx_compare(string1, string2 [,padchar])
#   This function will compare string1 to string2, and return a whole number
#   which will be 0 if they are equal, otherwise the position of the first
#   character at which the two strings differ is returned.
#   The comparison is case-sensitive, and leading and trailing space do matter.
#   If the strings are of unequal length, the shorter string will be padded at the
#   right hand end with the padchar character to the length of the longer string
#   before the comparison. If a padchar is not specified, <space> is used.
# ----------------------------------------------------------------------------
local string1="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local string2="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local padchar=" "
if [ "$3" != "" ]
then padchar="$(echo "$3" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
fi
local l1=${#string1}
local l2=${#string2}
if [ $l1 -gt $l2 ]
then string2="$string2$( rx_copies "$padchar" $(( $l1 - $l2 )) )"
elif [ $l2 -gt $l1 ]
then string1="$string1$( rx_copies "$padchar" $(( $l2 - $l1 )) )"
fi
if [ "$string1" = "$string2" ]
then echo 0
else cmp -b <(echo "$string1") <(echo "$string2") | cut -d',' -f1 | awk '{print $NF}'
fi
return 0
}

rx_compress() {
# ----------------------------------------------------------------------------
# rx_compress(string1, [list])
#   If the list argument is omitted,the function removes leading,trailing,
#   or embedded blank characters from the string argument.
#   If the optional list is supplied, it specifies the characters to be
#   removed from the string.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
if [ "$2" = "" ]
then local list=" "
else local list="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
fi
echo "$(echo "$string" | sed 's/'"$list"'//g')"
return $?
}

rx_copies() {
# ----------------------------------------------------------------------------
# rx_copies(str,n)
#   returns a string containing n copies of str
# ----------------------------------------------------------------------------
local str="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ $2 -lt 0 ] && local num=0 || local num=$2
yes "$str" | head -$num  | awk '{str=str$0} END {print str}'
return $?
}

rx_countstr() {
# ----------------------------------------------------------------------------
# rx_countstr(needle, haystack)
#   Returns a count of the number of occurrences of needle in haystack
#   needle can be a awk regexp, be aware correctly masquerading backslash character
#   e.g.
#     rx_countstr '\\\\' 'haystack with \\backslash'
#  will return 1
# ----------------------------------------------------------------------------
local needle="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local haystack="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
echo $(awk -v needle="$needle" -v haystack="$haystack" 'BEGIN {num=gsub(needle,"",haystack);print num}' < /dev/null)
}

rx_delstr() {
# ----------------------------------------------------------------------------
# rx_delstr(string, start [,length])
#   Returns string, after the substring of length length starting at position
#   start has been removed. The default value for length is the rest of the
#   string. Start must be a positive whole number, while length must be a
#   non-negative whole number.  It is not an error if start or length (or a
#   combination of them) refers to more characters than string holds
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local start="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local length=$(( $(rx_length "$string") - $start -1 )) || local length=$3
local length="$3"
# [ $(rx_length "string" ) -lt $length ] && string="$(rx_copies " " $length)"
local begin="$(rx_substr "$string" 1 $(( $start - 1 )) )"
local mid="$(rx_substr "$string" $start $length )"
local rest="$(rx_substr "$string" $(( $start + $(rx_length "$mid")  )) )"
echo "$begin$rest"
}

rx_delword() {
# ----------------------------------------------------------------------------
# rx_delword(string,start[,length])
#  Removes length words and all blanks between them, from string, starting at
#  word number start. The default value for length is the rest of the string.
#  All consecutive spaces will be removed. Nothing is removed if length is zero.
#
#  The valid range of start is the positive whole numbers; the first word in
#  string is numbered 1. The
#  valid range of length is the non-negative integers. It is not an error if
#  start or length (or a combination of them) refers to more words than string holds.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local start="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local length=$(( $(rx_words "$string") - $start -1 )) || local length=$3
local length="$3"
# [ $(rx_length "string" ) -lt $length ] && string="$(rx_copies " " $length)"
local begin="$(rx_subword "$string" 1 $(( $start - 1 )) )"
local mid="$(rx_subword "$string" $start $length )"
local rest="$(rx_subword "$string" $(( $start + $(rx_words "$mid")  )) )"
[ $start -gt 1 ] && local blank=" " || local blank=""
echo "$begin$blank$rest"
}

rx_index() {
# ----------------------------------------------------------------------------
# rx_index(haystack, needle [,start])
#   returns byte position of the first occurence of needle in haystack.
#   The search started at byte "start", returned is the absolute position in haystack
#   e.g. rx_index 'aaabbbccc' c 3 will return 7
#   if needle is not found starting at start, 0 is returned
#   needle can be a awk regexp
# ----------------------------------------------------------------------------
local needle="$(echo "$2" | sed "s/'/\[\'\]/g" | sed 's/"/\[\"\]/g')"
# echo "needle=$needle"
local haystack="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
# echo "haystack=$haystack"
if [ "$3" = "" ]
then local start=1
else local start=$3
fi
# the egrep for stderr is to avoid waring message around escaping apostrophe or quote
local pos=$( : | awk -v haystack="$haystack" -v needle="$needle" -v start=$start \
                   'BEGIN {print match(substr(haystack, start), needle)}' 2> >(egrep -vie '*[wW](arning|arnung)') )
local rc=$?
if [ $pos = 0 ]
then echo 0
else echo $(( $pos + $start -1 ))
fi
return $rc
}

rx_lastpos() {
# ----------------------------------------------------------------------------
# rx_lastpos(needle, haystack [,start])
#   Searches the string haystack for the string needle, and returns the
#   position in haystack of the first character in the substring that matched
#   needle. The search is started from the right side, so if needle occurs
#   several times, the last occurrence is reported.
#   If start is specified, the search starts at character number start in haystack.
#   Note that the standard only states that the search starts at the startth character.A
#   Note that regexp is not supported here
#   Note that start refers to the end !!
# ----------------------------------------------------------------------------
local needle="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
needle="$( echo "$needle" | rev )"
local haystack="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
if [ "$3" != "" ]
then haystack=$(rx_substr "$haystack" $3)
fi
haystack="$( echo "$haystack" | rev )"
local pos="$(awk -v needle="$needle" -v haystack="$haystack" 'BEGIN{print index(haystack,needle)}' </dev/null)"
if [ $pos = 0 ]
then echo 0
else
   pos=$(( $(rx_length "$haystack") - $pos + 1 ))
   if [ "$3" != "" ]
   then pos=$(( $pos + $3 - 1 ))
   fi
   echo $pos
fi
}

rx_left() {
# ----------------------------------------------------------------------------
# rx_left(string, length [,padchar])
#   Returns the length leftmost characters in string. If length (which must
#   be a non-negative whole number) is greater than the length of string,
#   the result is padded on the right with <space> (or padchar if that is
#   specified) to make it the correct length.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local length=$2
if [ "$3" = "" ]
then local padchar=" "
else local padchar="$3"
fi
local strlen=$(rx_length "$string")
local padlen=$(( $length - $strlen + 2))
if [ "$strlen" -gt $length ]
then echo "$(rx_substr "$string" 1 $length)"
elif [ "$strlen" = $length ]
then echo "$string"
else echo "$string" | sed -e :a -e 's/^.\{1,'$padlen'\}$/&'$padchar'/;ta'
fi
}

rx_length() {
# ----------------------------------------------------------------------------
# rx_length(string)
#   returns the length of a string
# ----------------------------------------------------------------------------
echo ${#1}
return $?
}

rx_lower() {
# ----------------------------------------------------------------------------
# rx_lower(string [,start [,length [,pad]]])
#   Translates the substring of string that starts at start, and has
#   the length length to lower case. Length defaults to the rest of the
#   string. Start must be a positive whole number, while length can be any
#   non-negative whole number. The default value for start is 1 and for length
#   is the length of string. It is not an error for start to be larger than
#   the length of string.  If length is specified and the sum of length and
#   start minus 1 is greater that the length of string, then the result will
#   be padded with padchars to the specified length.
#   The default value for padchar is the <space> character.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$2" = "" ] && local start=1 || local start=$2
[ "$3" = "" ] && local length="$(rx_length "$string")" || local length=$3
local pad="$(echo "$4" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$pad" = "" ] && pad=" "
local begin=""
[ $start -gt 1 ] && begin="$( rx_substr "$string" 1 $(( $start - 1)) )"
local str="$(rx_substr "$string" $start $length "$pad")"
local result="$(echo "$str" | tr '[A-ZÄÖÜ]' '[a-zäöü]')"
echo "$begin$result"
}

rx_max() {
# ----------------------------------------------------------------------------
# rx_max(number1 [,number2] ...)
#   Takes any positive number of parameters, and will return the parameter that
#   had the highest numerical value. The parameters may be any valid decimal number.
#   Have an eye on your locale, e.g. 0,1 for de_DE is 0.1 for en_US
# ----------------------------------------------------------------------------
echo "$@" | sed 's/[ ]\{1,\}/\n/g' | sort -n | tail -1
}

rx_min() {
# ----------------------------------------------------------------------------
# rx_min(number1 [,number2] ...)
#   Takes any positive number of parameters, and will return the parameter that
#   had the lowest numerical value. The parameters may be any valid decimal number.
#   Have an eye on your locale, e.g. 0,1 for de_DE is 0.1 for en_US
# ----------------------------------------------------------------------------
echo "$@" | sed 's/[ ]\{1,\}/\n/g' | sort -n | head -1
}

rx_overlay() {
# ----------------------------------------------------------------------------
# rx_overlay(string1, string2 [,[start] [,[length] [,padchar]]])
#  Returns a copy of string2, totally or partially overwritten by string1.
#  If these are the only arguments, the overwriting starts at the first
#  character in string2.
#  If start is specified, the first character in string1 overwrites character
#  number start in string2. Start must be a positive whole number, and
#  defaults to 1, i.e. the first character of string1.  If the start position
#  is to the right of the end of string2, then string2 is padded at the right
#  hand end to make it start-1 characters long, before string1 is added.
#  If length is specified, then string2 will be stripped or padded at the
#  right hand end to match the specified length. For padding (of both strings)
#  padchar will be used, or <space> if padchar is unspecified. Length must be
#  non-negative, and defaults to the length of string1.
# ----------------------------------------------------------------------------
local string1="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local string2="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local start=1 || local start=$3
[ "$4" = "" ] && local length=$(rx_length "$string2") || local length=$4
[ "$5" = "" ] && local padchar=" " || local padchar="$(echo "$5" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
string2="$( rx_substr "$string2" 1 $length )"
local begin="$( rx_substr "$string2" 1 $(( $start -1 )) )"
local begin_len=$( rx_length "$begin" )
local repl="$string1"
local repl_len=$( rx_length "$repl" )
local rest_len=$(( $length - $begin_len - $repl_len ))
local end="$( rx_substr "$string2" $(( $start + $( rx_length "$string1" ) )) $rest_len "$padchar" )"
echo "$begin$repl$end"
}

rx_pos() {
# ----------------------------------------------------------------------------
# rx_pos(needle, haystack [,start])
#   Seeks for an occurrence of the string needle in the string haystack.
#   If needle is not found, then 0 is returned.  Else, the position in haystack
#   of the first character in the part that matched is returned, which will be
#   a positive whole number.  If start (which must be a positive whole number)
#   is specified, the search for needle will start at position start in haystack.
# ----------------------------------------------------------------------------
local needle="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local haystack="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
if [ "$3" != "" ]
then haystack=$(rx_substr "$haystack" $3)
fi
local pos="$(awk -v needle="$needle" -v haystack="$haystack" 'BEGIN{print index(haystack,needle)}' </dev/null)"
if [ $pos = 0 ]
then echo 0
elif [ "$3" = "" ]
then echo $pos
elif [ $pos -lt $3 ]
then echo 0
else echo $(( $pos + $3 - 1 ))
fi
}

rx_reverse() {
# ----------------------------------------------------------------------------
# rx_reverse(string)
#   Returns a string of the same length as string, but having the order of the
#   characters reversed.
# ----------------------------------------------------------------------------
echo "$1" | rev
}

rx_right() {
# ----------------------------------------------------------------------------
# rx_right(string, length [,padchar])
#   Returns the length rightmost characters in string. If length (which must
#   be a non-negative whole number) is greater than the length of string,
#   the result is padded on the left with <space> (or padchar if that is
#   specified) to make it the correct length.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
string="$(echo "$string" | rev)"
local length=$2
if [ "$3" = "" ]
then local padchar=" "
else local padchar="$3"
fi
local strlen=$(rx_length "$string")
local padlen=$(( $length - $strlen + 2))
if [ "$strlen" -gt $length ]
then echo "$(rx_substr "$string" 1 $length)" | rev
elif [ "$strlen" = $length ]
then echo "$string" | rev
else echo "$string" | rev | sed -e :a -e 's/^.\{1,'$padlen'\}$/'$padchar'&/;ta'
fi
}

rx_space() {
# ----------------------------------------------------------------------------
# rx_space(string[, [length] [,padchar]])
#   With only one parameter string is returned, stripped of any trailing or
#   leading blanks, and any consecutive blanks inside string translated to a
#   single <space> character (or padchar if specified).
#   Length must be a non-negative whole number. If specified, consecutive blanks
#   within string are replaced by exactly length instances of <space> (or padchar
#   if specified).  However, padchar will only be used in the output string,
#   in the input string, blanks will still be the "magic" characters. As a
#   consequence, if there exist any padchars in string, they will remain
#   untouched and will not affect the spacing.
# ----------------------------------------------------------------------------
[ "$2" = ""  ] && local length=1 || local length=$2
[ "$3" = ""  ] && local padchar=" " || local padchar="$3"
padchar="$(rx_copies "$padchar" $length)"
echo "$1" | sed 's/[ ]\{1,\}/'"$padchar"'/g'
}

rx_strip() {
# ----------------------------------------------------------------------------
# rx_strip(string [,[option] [,char]])
#   Returns string after possibly stripping it of any number of leading and/or
#   trailing characters. The default action is to strip off both leading and
#   trailing blanks. If char (which must be a string containing exactly one
#   character) is specified, that character will be stripped off instead of blanks.
#   Inter-word blanks (or chars if defined, that are not leading of trailing) are
#   untouched.
#   If option is specified, it will define what to strip.
#   The possible values for option are:
#   [L] (Leading) Only strip off leading blanks, or chars if specified.
#   [T] (Trailing) Only strip off trailing blanks, or chars if specified.
#   [B] (Both) Combine the effect of L and T, that is, strip off both leading
#       and trailing blanks, or chars if it is specified. This is the default action.
# ----------------------------------------------------------------------------
[ "$2" = "" ] && local option="B" || local option=$2
[ "$3" = "" ] && local char=" " || local char="$3"
local reg=""
[ $option = "L" ] && reg='s/^['"$char"']\{1,\}//'
[ $option = "T" ] && reg='s/['"$char"']\{1,\}$//'
[ $option = "B" ] && reg='s/^['"$char"']\{1,\}//;s/['"$char"']\{1,\}$//'
echo "$1" | sed "$reg"
}

rx_substr() {
# ----------------------------------------------------------------------------
# rx_substr(string,start,[length],[padchar])
#   returns the substring of string beginning at position start with length
#   charaters in size. If the substr is shorter than length it
#   will be filled by padchar to the requested length
#   start is the position, not the offset (non-zero based)
#   length must be an integer
#   pad must be only 1 character (or copies of $4 are used and cut)
# ----------------------------------------------------------------------------
local str="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local start=$(( $2 - 1 ))
local length=$3
local padchar="$4"
if [ "$length" = "" ]
then length=$(( ${#str} - $start + 1 ))
else length=$3
fi
local fill="$(rx_copies "$padchar" $length)"
str="$str$fill"
echo "${str:$start:$length}"
}

rx_subword() {
# ----------------------------------------------------------------------------
# rx_subword(string, start [,length])
#   Returns the part of string that starts at blank delimited word start (which
#   must be a positive whole number). If length (which must be a non-negative
#   whole number) is specified, that number of words are returned. The default
#   value for length is the rest of the string.
#   Note: consecutive blanks are shrinked to a single space
# ----------------------------------------------------------------------------
[ "$3" = "" ] && local length="9999" || local length=$3
local start=$2
local found=0
local added=0
local processed=0
local result=""
for i in $1
do
   processed=$(($processed+1))
   if [ $processed -ge $start ]
   then
       if [ $added -lt $length ]
       then
          [[ "$result" = "" ]] && result="$i" || result="$result"" ""$i"
          added=$(($added+1))
       fi
    fi
done
echo "$result"
}

rx_upper() {
# ----------------------------------------------------------------------------
# rx_upper(number1 [,number2] ...)
#   Translates the substring of string that starts at start, and has
#   the length length to upper case. Length defaults to the rest of the
#   string. Start must be a positive whole number, while length can be any
#   non-negative whole number. The default value for start is 1 and for length
#   is the length of string. It is not an error for start to be larger than
#   the length of string.  If length is specified and the sum of length and
#   start minus 1 is greater that the length of string, then the result will
#   be padded with padchars to the specified length.
#   The default value for padchar is the <space> character.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$2" = "" ] && local start=1 || local start=$2
[ "$3" = "" ] && local length="$(rx_length "$string")" || local length=$3
local pad="$(echo "$4" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$pad" = "" ] && pad=" "
local begin=""
[ $start -gt 1 ] && begin="$( rx_substr "$string" 1 $(( $start - 1)) )"
local str="$(rx_substr "$string" $start $length "$pad")"
local result="$(echo "$str" | tr '[a-zäöü]' '[A-ZÄÖÜ]')"
echo "$begin$result"
}

rx_verify() {
# ----------------------------------------------------------------------------
# rx_verify(string, ref [,[option] [,start]])
#  With only the first two parameters, it will return the position of the first
#  character in string that is not also a character in the string ref. If all
#  characters in string are also in ref, it will return 0.
#  If option is specified, it can be one of:
#    [N] (Nomatch) - The result will be the position of the first character
#                    in string that does not exist in ref, or zero if all exist
#                    in ref. This is the default option.
#    [M] (Match) - Reverses the search, and returns the position of the first
#                  character in string that exists in ref. If none exists in
#                  ref, zero is returned.
#  If start (which must be a positive whole number) is specified, the search
#  will start at that position in string. The default value for start is 1.
# ----------------------------------------------------------------------------
local string="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local ref="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local option="N" || local option="$3"
[ "$4" = "" ] && local start=1 || local start=$4
# : | awk -v str="1523" -v chars="124" 'BEGIN { reg="[!"chars"]"; print match(str,/[^124]/) }'
local string_tobe_searched="$(rx_substr "$string" $start)"
local matchpos=0
if [ $option = "N" ]
then
   local awkstr='BEGIN{print match(str,/[^'$ref"]/)}"
   matchpos=$( : | awk -v str="$string_tobe_searched" "$awkstr" )
elif [ $option = "M" ]
then
   local awkstr='BEGIN{print match(str,/['$ref"]/)}"
   matchpos=$( : | awk -v str="$string_tobe_searched" "$awkstr" )
fi
[ $matchpos = 0 ] && echo 0 || echo $(( $matchpos + $start - 1 ))
}

rx_word() {
# ----------------------------------------------------------------------------
# rx_word(string, wordno)
#   Returns the blank delimited word number wordno from the string string.
#   If wordno (which must be a positive whole number) refers to a non-existing
#   word, then a nullstring is returned. The result will be stripped of any blanks.
# ----------------------------------------------------------------------------
rx_subword "$1" $2 1
}

rx_wordindex() {
# ----------------------------------------------------------------------------
# rx_wordindex(string, wordno)
#   Returns the character position of the first character of blank delimited
#   word number wordno in string, which is interpreted as a string of blank
#   delimited words.  If number (which must be a positive whole number) refers
#   to a word that does not exist in string, then 0 is returned.
# ----------------------------------------------------------------------------
local awkin='{ len=0;num=split($0,arr,FS,arrsep);for (i = 1; i <= word; i++) { len=len+length(arr[i])+length(arrsep[i-1]) };whole=len+length(arrsep[i])-length(arr[i]);print whole }'
echo "$1" | awk -v word=$2 "$awkin"
# pos $(word "$1" $2) "$1"
}

rx_wordlength() {
# ----------------------------------------------------------------------------
# rx_wordlength(string, wordno)
#   Returns the number of characters in blank delimited word number number in
#   string. If number (which must be a positive whole number) refers to an
#   non-existent word, then 0 is returned. Trailing or leading blanks do not
#   count when calculating the length.
# ----------------------------------------------------------------------------
rx_length $(rx_word "$1" $2)
}

rx_wordpos() {
# ----------------------------------------------------------------------------
# rx_wordpos(phrase, string [,start])
#   Returns the word number in string which indicates at which phrase begins,
#   provided that phrase is a subphrase of string. If not, 0 is returned to
#   indicate that the phrase was not found.
#   <start> means the word number to start with.
#   A phrase differs from a substring in one significant way; a phrase is a set
#   of WORDS, separated by any number of blanks.
#   For instance, "is  a" is a subphrase of  "This is a phrase".
#   Notice the different amount of whitespace between "is" and "a".
# ----------------------------------------------------------------------------
local phrase="$(echo "$1" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
local string="$(echo "$2" | sed "s/'/\\\'/g" | sed 's/"/\\\"/g')"
[ "$3" = "" ] && local start=1 || local start=$3
# compress the original string (remove duplicate blanks)
local new_string=$( rx_space "$string" )
# count the words
local words_new_string=$( rx_words "$new_string" )
# reduce the string on the left according to start
local str_tobe_compared="$( rx_subword "$new_string" $start )"
# compress the phrase (remove duplicate blanks)
local new_phrase="$( rx_space "$phrase" )"
# search for new_phrase in str_tobe_compared
local new_phrase_pos=$( rx_index "$str_tobe_compared " "$new_phrase" )
# get the remaining string
local rest_of_str_tobe_compared="$( rx_substr "$str_tobe_compared" $new_phrase_pos )"
# and count the words here
local words_of_rest=$( rx_words "$rest_of_str_tobe_compared" )
# We now know
#  1. the original number of words = <words_new_string>
#  2. number of word behind and including <new_phrase> = <words_of_rest>
# to calculate the word position we have to use
#  <words_new_string> - <words_new_string> + 1
[ $new_phrase_pos = 0 ] && echo 0 || echo $(( $words_new_string - $words_of_rest + 1 ))
}

rx_words() {
# ----------------------------------------------------------------------------
# rx_words(string)
#   Returns the number of blank delimited words in the string
# ----------------------------------------------------------------------------
local tmp=$(rx_subword "$1" 1)
local blanks=$( echo "$tmp" | awk '{print gsub(" ", " ", $0)}' )
[ "$tmp" = "" ] && echo 0 || echo $(($blanks+1))
}

# ========== Test cases =============
# rx_abbrev "Dumm" "Du"                        # Result: "1"
# rx_center "text" 10                          # Result: "   text   "
# rx_changestr "abc" "abd 123 abc def" "otto"  # Result: "abd 123 otto def"
# rx_compare abc ab c                          # Result: "0"
# rx_compress "a b  c "                        # Result: "abc"
# rx_copies x 10                               # Result: "xxxxxxxxxx"
# rx_countstr "abc" "abc 123 abc 456"          # Result: "2"
# rx_delstr "123456789" 2 2                    # Result: "1456789"
# rx_delword "eins zwei   drei vier" 2 2       # Result: "eins vier"
# rx_index "abcdefghi" "def"                   # Result: "4"
# rx_lastpos "ente" "ente huhn kuh ente pferd" # Result: "18"
# rx_left "123456789" 3                        # Result: "123"
# rx_length "123456789"                        # Result: "9"
# rx_lower "ABCdef" 2 9 "_"                    # Result: "Abcdef____"
# rx_max 1 2 9 5 6                             # Result: 9
# rx_min 1 2 9 0,1 5 6                         # Result: 0,1
# rx_overlay 'NEW' 'old-value'                 # Result: NEW-value
# rx_pos "abc" "123 abc 456"                   # Result: 5
# rx_reverse "abc"                             # Result: "cba"
# rx_right "123456789" 3                       # Result: "789"
# rx_space " eins   zwei drei   vier "         # Result: " eins zwei drei vier "
# rx_strip "  eins zwei   drei  "              # Result: #eins zwei   drei#
# rx_substr "to be or not to be" 4 5           # Result: "be or"
# rx_subword "to  be    or ot  to be" 2 2      # Result: "be or"
# rx_upper "abdDEF" 2 9 "_"                    # Result: "aBCDEF____"
# rx_verify "12 3" " 124"                      # Result: 4
# rx_word "to be or not to be" 3               # Result: "or"
# rx_wordindex "to be or not   to be" 5        # Result: "16"
# rx_wordlength "to be or not   to be" 4       # Result: "3"
# rx_wordpos "no" "to be or not   to be"  3    # Result: "4"
# rx_words "to be or not   to be"              # Result: "6"
